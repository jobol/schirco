(import (chicken irregex))

(define message
   (let* ((<letter>     'alpha)
          (<number>     'num)
          (<special>    '(/ "-[]\\`^{}"))
          (<SPACE>      '(+ #\x20))
          (<crlf>       '(seq #\x0d #\x0a))
          (<nonwhite>   '(~ #\x20 #\x00 #\x0d #\x0a))
          (<chstring>   '(+ (~ #\x20 #\x07 #\x00 #\x0d #\x0a #\,)))
          (<trailing>   '(* (~ #\x00 #\x0d #\x0a)))
          (<middle>     `(seq (~ #\x20 #\x00 #\x0d #\x0a #\:) (* ,<nonwhite>)))
          (<user>       `(+ ,<nonwhite>))
          (<mask>       `(seq (/ #\# #\$) ,<chstring>))
          (<nick>       `(seq ,<letter> (* (or ,<letter> ,<number> ,<special>))))
          (<host>       "todo")
          (<servername> <host>)
          (<channel>    `(seq (/ #\# #\&) ,<chstring>))
          (<to>         `(or ,<channel> (seq ,<user> #\@ ,<servername>) ,<nick> ,<mask>))
          (<target>     `(seq ,<to> (* #\, ,<to>)))
          (<command>    `(or (+ ,<letter>) (= 3 ,<number>)))
          (<prefix>     `(or ,<servername> (seq ,<nick> (? #\! ,<user>) (? #\@ ,<host>))))
          (<params>     `(seq ,<SPACE> (* (=> param ,<middle>) ,<SPACE>) (? #\: (=> param ,<trailing>))))
          (<message>    `(seq (=> prefix (? #\: ,<prefix> ,<SPACE>)) (=> command ,<command>) ,<params>)) ; ,<crlf>))
)
   (irregex <message>)))


;> (define y (irregex-match message ":todo 456 toto tata titi :edroij reouiher"))
;> (irregex-match-substring y 0)
;":todo 456 toto tata titi :edroij reouiher"
;> (irregex-match-substring y 1)
;":todo "
;> (irregex-match-substring y 2)
;"456"
;> (irregex-match-substring y 3)
;"titi"
;> (irregex-match-substring y 4)
;"edroij reouiher"

(define-record-type <message>
   (make-message tags source command parameters)
   message?
   (tags       message-tags)
   (source     message-source)
   (command    message-command)
   (parameters message-parameters))

(define (message->alist msg)
   `((tags . ,(message-tags msg))
     (source . ,(message-source msg))
     (command . ,(message-command msg))
     (parameters . ,(message-parameters msg))))

(define message-rex
   (irregex '(seq (? "@" ($ (+ (~ " "))) (+ " "))
                  (? ":" ($ (+ (~ " "))) (+ " "))
                  ($ (+ alnum))
                  (? (+ " ") ($ (+ any))))))

(define (msgstr->message str)
  (cond
    ((irregex-match message-rex str)
       => (lambda (m)
             (make-message
                (irregex-match-substring m 1)
                (irregex-match-substring m 2)
                (irregex-match-substring m 3)
                (irregex-match-substring m 4))))
    (else #f)))

(define (msgstr->alist str)
  (cond
    ((msgstr->message str) => message->alist)
    (else #f)))

;----------------------------------------------------------------
; tagstr->alist: translate a string encoding tags to an alist
;
; example:
;   (tagstr->alist "toto=tat;78=ert;rete;zer=grege")
;    -> ((zer . "grege") (rete . #f) (|78| . "ert") (toto . "tat"))
;   (tagstr->alist "toto=tat;78=e\\s\\:\\\\rt;rete;zer=grege")
;    -> ((zer . "grege") (rete . #f) (|78| . "e ;\\rt") (toto . "tat"))
(define (tagstr->alist str)
  (letrec (
     (len (string-length str))
     (take (lambda (beg end)
              (substring str beg end)))
     (take-key (lambda (beg end)
              (string->symbol (take beg end))))
     (esc? (lambda (c)
              (char=? c #\\)))
     (unscape-count-esc (lambda (idx end cnt)
              (if (= idx end)
                 cnt
                 (unscape-count (+ idx 1) end (+ cnt 1)))))
     (unscape-count (lambda (idx end cnt)
              (cond
                 ((= idx end) cnt)
                 ((esc? (string-ref str idx)) (unscape-count-esc (+ idx 1) end cnt))
                 (else    (unscape-count (+ idx 1) end (+ cnt 1))))))
     (unscaped-char (lambda (c)
              (case c ((#\:) #\;) ((#\r) #\return) ((#\n) #\newline) ((#\s) #\space) (else c))))
     (unscape-to-esc (lambda (idx end to j)
              (cond
                 ((= idx end) to)
                 (else
                    (string-set! to j (unscaped-char (string-ref str idx)))
                    (unscape-to (+ idx 1) end to (+ j 1))))))
     (unscape-to (lambda (idx end to j)
              (if (= idx end)
                 to
                 (let ((c (string-ref str idx)))
                    (if (esc? c)
                       (unscape-to-esc (+ idx 1) end to j)
                       (begin
                         (string-set! to j c)
                         (unscape-to (+ idx 1) end to (+ j 1))))))))
     (unscape (lambda (beg end)
                 (let* (
                    (cnt (unscape-count beg end 0)))
                    (if (zero? (- end beg cnt))
                       (take beg end)
                       (unscape-to beg end (make-string cnt) 0)))))
     (push (lambda (k beg end r)
              (if k
                 (cons (cons k (unscape beg end)) r)
                 (if (= beg end)
                    r
                    (cons (cons (take-key beg end) #f) r)))))
     (loop (lambda (idx r k beg)
              (if (= idx len)
                 (push k beg idx r)
                 (let (
                     (c (string-ref str idx))
                     (nidx (+ idx 1)))
                   (case c
                      ((#\;) (loop nidx (push k beg idx r) #f nidx))
                      ((#\=) (if k (loop nidx r k beg) (loop nidx r (take-key beg idx) nidx)))
                      (else  (loop nidx r k beg))))))))
  (loop 0 '() #f 0)))

;----------------------------------------------------------------
; paramstr->list: translate a string encoding parameters to a list
;
; examples:
;  (paramstr->list "    ")
;   -> ()
;  (paramstr->list "alpha beta  gamma   delta   ")
;   -> ("alpha" "beta" "gamma" "delta")
;  (paramstr->list "alpha beta  :gamma   delta   ")
;   -> ("alpha" "beta" "gamma   delta   ")
(define (paramstr->list str)
   (letrec (
       (len (string-length str))
       (take (lambda (beg end)
              (substring str beg end)))
       (push (lambda (beg end r)
              (if (= beg end)
                 r
                 (cons (take beg end) r))))
       (loop (lambda (idx r beg)
              (if (= idx len)
                 (reverse (push beg idx r))
                 (let (
                     (c (string-ref str idx))
                     (nidx (+ idx 1)))
                   (case c
                      ((#\:)     (reverse (push nidx len r)))
                      ((#\space) (loop nidx (push beg idx r) nidx))
                      (else      (loop nidx r beg))))))))
     (loop 0 '() 0)))

